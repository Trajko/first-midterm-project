#include <stdio.h>
#include <math.h>

//Functions

void userChoice();
void assignFloats();
void makeMenu();
void calculate();

//Declaring floats

float float1, float2, result;

//Declaring integers

int keepWorking = 1, option, nums;

//Declaring strings

char option1[] = "Sum";
char option2[] = "Difference";
char option3[] = "Product";
char option4[] = "Ratio";
char option5[] = "Sum of squares";
char option6[] = "Sum of cubes";
char option7[] = "Ratio of squares";
char option8[] = "Ratio of cubes";

void main() {

		//Displaying menu
		makeMenu();

		//user choice
		userChoice();

		//calling function to display text and assign floats
		assignFloats();

		//calling function to calculate and display resault
		calculate();

		//After initial run asking user if he would like to continue working or end the program
		lastCalculation: //going back here after first execution until user chooses to exit
		printf("Would you like to keep working or end the program?\n");
		printf("Enter \"1\" if you would like to keep working, or any other number to exit the program.\n");

		//Listening to the input
		scanf_s("%d", &keepWorking);

		//Main if/else: Checking if user wanted to keep working
		//Than if user decided to keep working displaying menu again, asking him what calculation he wants to choose and finally asking him if he wants to use old or enter new numbers
		//Nested if/else inside main if: checking if user wanted to use new numbers, if so function to assign new numbers is called followed by calculation, otherwise just calculation is called

		if (keepWorking == 1) {
			
			makeMenu();
			userChoice();
			printf("Would you like to enter new floats or use the ones you've entered before?\n");
			printf("If you want to use new floats enter \"1\" or any other number if you want to just calculate: ");
			scanf_s("%d", &nums);

			if (nums == 1) {

				assignFloats();
				calculate();
				goto lastCalculation; //Using goto function is much easier, more efficient and better for control flow than looping

			}

			else
				calculate();
				goto lastCalculation;


		}

		else {
			exit(0);
		}




	return 0;
}

//Function used to scan user's choice and later make shure user's input (choice) is valid
void userChoice() {

	//Asking user to select calculation
	//defining userChoice in code so if user entered invalid number he can try again
userChoice:
	printf("Enter the number of calculation: ");

	//Scanning
	scanf_s("%d", &option);

	//Switch just to output what calculation user has selected 
	switch (option) {

	case 1:
		printf("You have selected %s\n", option1);
		break;

	case 2:
		printf("You have selected %s\n", option2);
		break;

	case 3:
		printf("You have selected %s\n", option3);
		break;

	case 4:
		printf("You have selected %s\n", option4);
		break;

	case 5:
		printf("You have selected %s\n", option5);
		break;

	case 6:
		printf("You have selected %s\n", option6);
		break;

	case 7:
		printf("You have selected %s\n", option7);
		break;

	case 8:
		printf("You have selected %s\n", option8);
		break;

	default:
		printf("Oops, looks like you have entered the number that does not belong to any option, try again.\n");
		goto userChoice;
		break;
	}

	
}

//Assigning user's inputed floats to variables

void assignFloats() {

	printf("Now enter the first float: ");
	scanf_s("%f", &float1);
	printf("Now enter the second float: ");
	scanf_s("%f", &float2);
	printf("\n");

}

void makeMenu() {
	//Design of the menu
	//Loop could be used to generate line for each option but it's really unnecessary
	// and it's more work to make loop do the spacing and aligning also

	printf("*=====================================*\n");
	printf("|               Welcome,              |\n");
	printf("|                                     |\n");
	printf("|  Below are the caltulations you can |\n");
	printf("|  choose, select one and hit Enter.  |\n");
	printf("|                                     |\n");
	printf("|  1. %s                             |\n", option1);
	printf("|  2. %s                      |\n", option2);
	printf("|  3. %s                         |\n", option3);
	printf("|  4. %s                           |\n", option4);
	printf("|  5. %s                  |\n", option5);
	printf("|  6. %s                    |\n", option6);
	printf("|  7. %s                |\n", option7);
	printf("|  8. %s                  |\n", option8);
	printf("*=====================================*\n");
	printf("\n");
	//end of menu
}

//calculating and displaying based on option and numbers user has entered
void calculate() {
	switch (option) {

	case 1:
		result = float1 + float2;
		printf("%s of floats is %0.2f\n", option1, result);
		break;

	case 2:
		result = float1 - float2;
		printf("%s of floats is %0.2f\n", option2, result);
		break;

	case 3:
		result = float1 * float2;
		printf("%s of floats is %0.2f\n", option3, result);
		break;

	case 4:
		result = float1 / float2;
		printf("%s of floats is %0.2f\n", option4, result);
		break;

	case 5:
		result = pow(float1, 2) + pow(float2, 2);
		printf("%s of floats is %0.2f\n", option5, result);
		break;

	case 6:
		result = pow(float1, 3) + pow(float2, 3);
		printf("%s of floats is %0.2f\n", option6, result);

	case 7:
		result = pow(float1, 2) / pow(float2, 2);
		printf("%s of floats is %0.2f\n", option7, result);
		break;

	case 8:
		result = pow(float1, 3) / pow(float2, 3);
		printf("%s of floats is %0.2f\n", option8, result);
		break;

	default:
		break;
	}
}